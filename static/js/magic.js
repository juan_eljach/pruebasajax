function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

function AddMessage(event){
   var csrftoken = getCookie('csrftoken');
   var title = $('#title').val();
   var message = $('#message').val();
   var priority = $('#priority').val();
   var meeting = $('#meeting').val();
   var url = "/item/add/";
   $.ajax({
       type: "POST",
       url: url,
       data: {
           'title': title,
           'message': message,
           'priority': priority,
           'meeting': meeting,
           'csrfmiddlewaretoken': csrftoken
       },
       success: function(data){
        console.log("SUCCESSSSSSSSSSSSSSSSSSSSS");
       },
   });
}