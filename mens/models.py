from django.db import models

PRIORITY_CHOICES = (
	(1, 'Low'),
	(2, 'Normal'),
	(3, 'High'),
)

class Meeting(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class Message(models.Model):
	title = models.CharField(max_length=100)
	message = models.TextField(max_length=1000)
	priority = models.IntegerField(choices=PRIORITY_CHOICES, default=0)
	done = models.BooleanField(default=False)
	created = models.DateTimeField(auto_now_add=True)
	meeting = models.ForeignKey(Meeting)

	def __str__(self):
		return self.title