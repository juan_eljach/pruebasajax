from django.contrib import admin
from .models import Message, Meeting

admin.site.register(Message)
admin.site.register(Meeting)
# Register your models here.
