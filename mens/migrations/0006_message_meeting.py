# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mens', '0005_meeting'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='meeting',
            field=models.ForeignKey(default=1, to='mens.Meeting'),
            preserve_default=False,
        ),
    ]
