# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mens', '0002_message_priority'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='done',
            field=models.BooleanField(default=False),
        ),
    ]
