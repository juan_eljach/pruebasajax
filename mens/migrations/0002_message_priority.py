# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mens', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='priority',
            field=models.IntegerField(choices=[(1, 'Low'), (2, 'Normal'), (3, 'High')], default=0),
        ),
    ]
