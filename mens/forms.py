#from django import forms
from django.forms import ModelForm
from .models import Message


class AddMessageForm(ModelForm):
    class Meta:
        model = Message
        exclude = ("meeting", )
        fields = ["title", "message", "priority"]
