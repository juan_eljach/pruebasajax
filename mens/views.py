#from django.shortcuts import render
from django.views.generic import CreateView, ListView, DetailView
from .utils import render_to_json_response
from .forms import AddMessageForm
from .models import Message, Meeting


class MessagesListView(ListView):
    model = Message
    template_name = "messages.html"
    context_object_name = "messages"

    def get_queryset(self):
        return Message.objects.all()


class MeetingDetailView(DetailView):
    model = Meeting
    template_name = "meeting_detail.html"
    context_object_name = "meeting"

    def get_context_data(self, *args, **kwargs):
        context = super(MeetingDetailView, self).get_context_data(
            *args, **kwargs)
        obj = self.get_object()
        context['messages'] = obj.message_set.all()
        return context

    def get_object(self, *args, **kwargs):
        meeting_pk = self.kwargs["meeting_pk"]
        obj = self.model.objects.get(pk=meeting_pk)
        return obj


class MeetingListView(ListView):
    model = Meeting
    template_name = "meeting_list.html"
    context_object_name = "meetings"

    def get_queryset(self):
        return Meeting.objects.all()


class AjaxFormResponseMixin(object):

    def form_invalid(self, form):
        print ("Invalid shit")
        print (form.errors)
        return render_to_json_response(form.errors, status=400)

    def form_valid(self, form):
        print ("Entrando al valid")
        form.instance.meeting = Meeting.objects.first()
        # save
        self.object = form.save()

        # initialize an empty context
        context = {}

        # return the context as json
        return render_to_json_response(self.get_context_data(context))


class AjaxItemCreateView(AjaxFormResponseMixin, CreateView):
    form_class = AddMessageForm

    def get_context_data(self, context):
        context['success'] = True
        context['title'] = self.object.title
        context['message'] = self.object.message
        print (self.object.title)
        print (self.object.message)
        return context